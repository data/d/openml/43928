# OpenML dataset: medical_charges

https://www.openml.org/d/43928

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The Medicare Inpatient Hospitals by Provider and Service dataset provides information on inpatient discharges for Original Medicare Part A beneficiaries by IPPS hospitals. It includes information on the use, payment, and hospital charges for more than 3,000 U.S. hospitals that received IPPS payments. The data are organized by hospital and Medicare Severity Diagnosis Related Group (DRG). The DRGs included in this dataset represent more than seven million discharges or 75% of total Medicare IPPS discharges.Data from 2019 is used. Avg_Tot_Pymt_Amt and Avg_Submtd_Cvrd_Chrg columns are dropped

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43928) of an [OpenML dataset](https://www.openml.org/d/43928). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43928/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43928/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43928/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

